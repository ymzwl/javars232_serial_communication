package test;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;

import gnu.io.CommPort;
import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;

public class Test {
	public static void main(String[] args) {
		OutputStream out = null;
		SerialPort serialPort = null;
		try {
			 //通过端口名识别端口
            CommPortIdentifier portIdentifier = CommPortIdentifier.getPortIdentifier("COM1");
            //打开端口，并给端口名字和一个timeout（打开操作的超时时间）
            CommPort commPort = portIdentifier.open("COM1", 2000);
            //判断是不是串口
            if (commPort instanceof SerialPort) {
                serialPort = (SerialPort) commPort;
                //设置一下串口的波特率等参数 baudrate
                serialPort.setSerialPortParams(9600, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);                              
            }        
            out = serialPort.getOutputStream();
            
            for(int i=0;i<10000;i++) {
            	Thread.sleep(1000);
            	String time=String.valueOf(new Date().getTime());
            	System.out.println(time);
				out.write(time.getBytes());
				out.flush();
            }
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (out != null) {
					out.close();
					out = null;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
