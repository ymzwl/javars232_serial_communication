package test;

import rs232Test.Util;

public class NumberTest{
	 // 16转10计算
    public static long getNum(String num1, String num2) {
        long value = Long.parseLong(num1, 16) * 256 + Long.parseLong(num2, 16);
        return value;
    }
    /**
     * 数组转换成十六进制字符串
     * @param byte[]
     * @return HexString
     */
    public static final String bytesToHexString(byte[] bArray) {
        StringBuffer sb = new StringBuffer(bArray.length);
        String sTemp;
        for (int i = 0; i < bArray.length; i++) {
            sTemp = Integer.toHexString(0xFF & bArray[i]);
            if (sTemp.length() < 2)
                sb.append(0);
            sb.append(sTemp.toUpperCase());
        }
        return sb.toString();
    }
    public static void strLengthTest() {
    	System.out.println("123".length());
    }
	public static void main(String[] args) {
		//byte[] uploadData= {(byte)0xFF,(byte)0xFF,(byte)0x2C,(byte)0x00,(byte)0x00,(byte)0x10,(byte)0x00,(byte)0x00,(byte)0x00,(byte)0x00,(byte)0x00,(byte)0x00,(byte)0x00,(byte)0x00,(byte)0x00,(byte)0x00,(byte)0x00,(byte)0x00,(byte)0x00,(byte)0x00,(byte)0x00,(byte)0x00,(byte)0x00,(byte)0x00,(byte)0x00,(byte)0x00,(byte)0x00,(byte)0x00,(byte)0x00,(byte)0x00,(byte)0x00,(byte)0x00,(byte)0x00,(byte)0x00,(byte)0x00,(byte)0x00,(byte)0x00,(byte)0x00,(byte)0x00,(byte)0x00,(byte)0x00,(byte)0x00,(byte)0xFF,(byte)0xF7};
		//byte[] b= {(byte)0xFF,(byte)0xFF};
		//byte[] b= {(byte)0x9,(byte)0x8};
		//System.out.println(b[0]);
		byte a=(byte)0x11;
    	byte b=(byte)0x10;
    	byte c=(byte)(a^a);
    	byte d=(byte)(b&b);
    	System.out.println(c);
    	System.out.println(d);
    	//byte d=(byte)(a&b);
		//System.out.println(Util.byte2bits(c));
		//System.out.println(Util.byte2bits(d));
//		System.out.println(a&b);
		//String s="123";
		//System.out.println(s.substring(1));
	}
}
