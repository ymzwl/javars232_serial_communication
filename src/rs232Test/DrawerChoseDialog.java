package rs232Test;

import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

/**
 * @author Administrator
 * 抽屉设置弹出框
 */
public class DrawerChoseDialog extends Dialog {

	protected Object result;
	protected Shell shell;
	String drawerData;

	/**
	 * Create the dialog.
	 * @param parent
	 * @param style
	 */
	public DrawerChoseDialog(Shell parent, int style,String drawerData) {
		super(parent, style);
		setText("SWT Dialog");
		this.drawerData=drawerData;
	}

	/**
	 * Open the dialog.
	 * @return the result
	 */
	public Object open() {
		createContents();
		shell.open();
		shell.layout();
		Display display = getParent().getDisplay();
		shell.setLocation(getParent().getLocation());
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		return result;
	}

	/**
	 * Create contents of the dialog.
	 */
	private void createContents() {
		shell = new Shell(getParent(), getStyle());
		shell.setSize(496, 190);
		shell.setText("抽屉设置");
		
		Button btnCheckButton = new Button(shell, SWT.CHECK);
		btnCheckButton.setText("抽屉1");
		btnCheckButton.setBounds(10, 10, 86, 30);
		
		Button btnCheckButton_1 = new Button(shell, SWT.CHECK);
		btnCheckButton_1.setText("抽屉2");
		btnCheckButton_1.setBounds(102, 10, 86, 30);
		
		Button btnCheckButton_2 = new Button(shell, SWT.CHECK);
		btnCheckButton_2.setText("抽屉3");
		btnCheckButton_2.setBounds(194, 10, 86, 30);
		
		Button btnCheckButton_3 = new Button(shell, SWT.CHECK);
		btnCheckButton_3.setText("抽屉4");
		btnCheckButton_3.setBounds(286, 10, 86, 30);
		
		Button btnCheckButton_4 = new Button(shell, SWT.CHECK);
		btnCheckButton_4.setText("抽屉5");
		btnCheckButton_4.setBounds(378, 10, 86, 30);
		
		Button btnCheckButton_9 = new Button(shell, SWT.CHECK);
		btnCheckButton_9.setText("抽屉10");
		btnCheckButton_9.setBounds(378, 46, 86, 30);
		
		Button btnCheckButton_8 = new Button(shell, SWT.CHECK);
		btnCheckButton_8.setText("抽屉9");
		btnCheckButton_8.setBounds(286, 46, 86, 30);
		
		Button btnCheckButton_7 = new Button(shell, SWT.CHECK);
		btnCheckButton_7.setText("抽屉8");
		btnCheckButton_7.setBounds(194, 46, 86, 30);
		
		Button btnCheckButton_6 = new Button(shell, SWT.CHECK);
		btnCheckButton_6.setText("抽屉7");
		btnCheckButton_6.setBounds(102, 46, 86, 30);
		
		Button btnCheckButton_5 = new Button(shell, SWT.CHECK);
		btnCheckButton_5.setText("抽屉6");
		btnCheckButton_5.setBounds(10, 46, 86, 30);
		
		Button btnNewButton = new Button(shell, SWT.NONE);
		btnNewButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				String drawerCode="";
				if(btnCheckButton.getSelection()) {
					drawerCode+="1";
				}
				if(!btnCheckButton.getSelection()) {
					drawerCode+="0";
				}
				
				if(btnCheckButton_1.getSelection()) {
					drawerCode+="1";
				}
				if(!btnCheckButton_1.getSelection()) {
					drawerCode+="0";
				}
				
				if(btnCheckButton_2.getSelection()) {
					drawerCode+="1";
				}
				if(!btnCheckButton_2.getSelection()) {
					drawerCode+="0";
				}
				
				if(btnCheckButton_3.getSelection()) {
					drawerCode+="1";
				}
				if(!btnCheckButton_3.getSelection()) {
					drawerCode+="0";
				}
				
				if(btnCheckButton_4.getSelection()) {
					drawerCode+="1";
				}
				if(!btnCheckButton_4.getSelection()) {
					drawerCode+="0";
				}
				
				if(btnCheckButton_5.getSelection()) {
					drawerCode+="1";
				}
				if(!btnCheckButton_5.getSelection()) {
					drawerCode+="0";
				}
				
				if(btnCheckButton_6.getSelection()) {
					drawerCode+="1";
				}
				if(!btnCheckButton_6.getSelection()) {
					drawerCode+="0";
				}
				
				if(btnCheckButton_7.getSelection()) {
					drawerCode+="1";
				}
				if(!btnCheckButton_7.getSelection()) {
					drawerCode+="0";
				}
				
				if(btnCheckButton_8.getSelection()) {
					drawerCode+="1";
				}
				if(!btnCheckButton_8.getSelection()) {
					drawerCode+="0";
				}
				
				if(btnCheckButton_9.getSelection()) {
					drawerCode+="1";
				}
				if(!btnCheckButton_9.getSelection()) {
					drawerCode+="0";
				}
				
				//System.out.println(drawerCode);
				Control.openCloseDrawer(drawerCode);
				
				
			}
		});
		btnNewButton.setBounds(137, 98, 98, 30);
		btnNewButton.setText("确定");
		
		Button btnNewButton_1 = new Button(shell, SWT.NONE);
		btnNewButton_1.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				Control.btnNewButton_3.setEnabled(true);
				shell.dispose();
				//shell.getParent().setEnabled(true);
			}
		});
		btnNewButton_1.setText("取消");
		btnNewButton_1.setBounds(241, 98, 98, 30);
		
		if(this.drawerData.substring(0, 1).equals("0")) {
			btnCheckButton.setSelection(false);
		}
		if(this.drawerData.substring(0, 1).equals("1")) {
			btnCheckButton.setSelection(true);
		}
		
		if(this.drawerData.substring(1, 2).equals("0")) {
			btnCheckButton_1.setSelection(false);
		}
		if(this.drawerData.substring(1, 2).equals("1")) {
			btnCheckButton_1.setSelection(true);
		}
		
		if(this.drawerData.substring(2, 3).equals("0")) {
			btnCheckButton_2.setSelection(false);
		}
		if(this.drawerData.substring(2, 3).equals("1")) {
			btnCheckButton_2.setSelection(true);
		}
		
		if(this.drawerData.substring(3, 4).equals("0")) {
			btnCheckButton_3.setSelection(false);
		}
		if(this.drawerData.substring(3, 4).equals("1")) {
			btnCheckButton_3.setSelection(true);
		}
		
		if(this.drawerData.substring(4, 5).equals("0")) {
			btnCheckButton_4.setSelection(false);
		}
		if(this.drawerData.substring(4, 5).equals("1")) {
			btnCheckButton_4.setSelection(true);
		}
		
		if(this.drawerData.substring(5, 6).equals("0")) {
			btnCheckButton_5.setSelection(false);
		}
		if(this.drawerData.substring(5, 6).equals("1")) {
			btnCheckButton_5.setSelection(true);
		}
		
		if(this.drawerData.substring(6, 7).equals("0")) {
			btnCheckButton_6.setSelection(false);
		}
		if(this.drawerData.substring(6, 7).equals("1")) {
			btnCheckButton_6.setSelection(true);
		}
		
		if(this.drawerData.substring(7, 8).equals("0")) {
			btnCheckButton_7.setSelection(false);
		}
		if(this.drawerData.substring(7, 8).equals("1")) {
			btnCheckButton_7.setSelection(true);
		}
		
		if(this.drawerData.substring(8, 9).equals("0")) {
			btnCheckButton_8.setSelection(false);
		}
		if(this.drawerData.substring(8, 9).equals("1")) {
			btnCheckButton_8.setSelection(true);
		}
		
		if(this.drawerData.substring(9, 10).equals("0")) {
			btnCheckButton_9.setSelection(false);
		}
		if(this.drawerData.substring(9, 10).equals("1")) {
			btnCheckButton_9.setSelection(true);
		}
		shell.getParent().setEnabled(true);
	}
}
