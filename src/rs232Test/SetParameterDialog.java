package rs232Test;

import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Text;

/**
 * @author Administrator
 * 抽屉设置弹出框
 */
public class SetParameterDialog extends Dialog {

	protected Object result;
	protected Shell shell;
	String drawerData;
	private Text text;
	private Text text_2;
	private Text text_3;
	private Text text_4;
	private Text text_5;

	/**
	 * Create the dialog.
	 * @param parent
	 * @param style
	 */
	public SetParameterDialog(Shell parent, int style) {
		super(parent, style);
		setText("SWT Dialog");
	}

	/**
	 * Open the dialog.
	 * @return the result
	 */
	public Object open() {
		createContents();
		shell.open();
		shell.layout();
		Display display = getParent().getDisplay();
		shell.setLocation(getParent().getLocation());
		
		Label lblNewLabel = new Label(shell, SWT.NONE);
		lblNewLabel.setBounds(10, 22, 126, 30);
		lblNewLabel.setText("设备ID");
		
		text = new Text(shell, SWT.BORDER);
		text.setBounds(203, 19, 163, 33);
		
		Label lblNewLabel_1_1 = new Label(shell, SWT.NONE);
		lblNewLabel_1_1.setText("设备地址");
		lblNewLabel_1_1.setBounds(9, 80, 127, 30);
		
		Label lblNewLabel_1_1_1 = new Label(shell, SWT.NONE);
		lblNewLabel_1_1_1.setText("采集时间间隔");
		lblNewLabel_1_1_1.setBounds(9, 141, 127, 30);
		
		Label lblNewLabel_1_1_1_1 = new Label(shell, SWT.NONE);
		lblNewLabel_1_1_1_1.setText("压缩机启动延时");
		lblNewLabel_1_1_1_1.setBounds(9, 203, 127, 30);
		
		Label lblNewLabel_1_1_1_1_1 = new Label(shell, SWT.NONE);
		lblNewLabel_1_1_1_1_1.setText("温度控制偏差");
		lblNewLabel_1_1_1_1_1.setBounds(9, 253, 127, 30);
		
		text_2 = new Text(shell, SWT.BORDER);
		text_2.setBounds(201, 77, 163, 33);
		
		text_3 = new Text(shell, SWT.BORDER);
		text_3.setBounds(203, 138, 163, 33);
		
		text_4 = new Text(shell, SWT.BORDER);
		text_4.setBounds(203, 200, 163, 33);
		
		text_5 = new Text(shell, SWT.BORDER);
		text_5.setBounds(203, 250, 163, 33);
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		return result;
	}

	/**
	 * Create contents of the dialog.
	 */
	private void createContents() {
		shell = new Shell(getParent(), getStyle());
		shell.setSize(418, 461);
		shell.setText("设置参数");
		
		Button btnNewButton = new Button(shell, SWT.NONE);
		btnNewButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				try {
					byte[] setByte= {(byte)0xFF, (byte)0xFF ,(byte)0x1C ,(byte)0x75 ,(byte)0x7F ,(byte)0x05 ,(byte)0x10 ,(byte)0x01 ,(byte)0x25 ,(byte)0xF0 ,(byte)0x02 ,(byte)0x01 ,(byte)0x0A ,(byte)0x02 ,(byte)0x05 ,(byte)0x07 ,(byte)0x08 ,(byte)0x08 ,(byte)0x02 ,(byte)0xFF ,(byte)0x03 ,(byte)0xFF ,(byte)0x03 ,(byte)0x20 ,(byte)0x36 ,(byte)0xE8 ,(byte)0xFF ,(byte)0xF7 ,(byte)0xFF ,(byte)0xFF ,(byte)0x1C ,(byte)0x75 ,(byte)0x7F ,(byte)0x05 ,(byte)0x10 ,(byte)0x01 ,(byte)0x25 ,(byte)0xF0 ,(byte)0x02 ,(byte)0x01 ,(byte)0x0A ,(byte)0x02 ,(byte)0x05 ,(byte)0x07 ,(byte)0x08 ,(byte)0x08 ,(byte)0x02 ,(byte)0xFF ,(byte)0x03 ,(byte)0xFF ,(byte)0x03 ,(byte)0x20 ,(byte)0x36 ,(byte)0xE8 ,(byte)0xFF ,(byte)0xF7};
					byte[] idByte=Util.intToByteArray(Integer.parseInt(text.getText()));
					setByte[6]=idByte[0];
					setByte[7]=idByte[1];
					setByte[8]=idByte[2];
					setByte[9]=idByte[3];
					setByte[11]=(byte)Integer.parseInt(text_2.getText());
					Control.outData(setByte);
				} catch (Exception e2) {
					MessageBox dialog=new MessageBox(shell,SWT.OK|SWT.ICON_INFORMATION);
			        dialog.setText("提示");
			        dialog.setMessage("输入错误！");
			        dialog.open();
				}
				
			}
		});
		btnNewButton.setBounds(97, 337, 98, 30);
		btnNewButton.setText("确定");
		
		Button btnNewButton_1 = new Button(shell, SWT.NONE);
		btnNewButton_1.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				Control.btnNewButton_3.setEnabled(true);
				shell.dispose();
				//shell.getParent().setEnabled(true);
			}
		});
		btnNewButton_1.setText("取消");
		btnNewButton_1.setBounds(201, 337, 98, 30);
		
	}
}
